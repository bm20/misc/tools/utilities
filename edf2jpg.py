import os
import sys
import glob
import argparse

import numpy as np

import cv2
import silx
from silx import io as silx_io

parser = argparse.ArgumentParser(description='Convert edf to jpg.')
parser.add_argument('input_f', metavar='input_f', type=str, nargs='+',
                    help='a list of edf files or a directory')
parser.add_argument('-o',
                    dest='outdir',
                    metavar='outdir',
                    nargs=1,
                    default='.',
                    help='output directory')
parser.add_argument('-f',
                    dest='force',
                    action='store_true',
                    default=False,
                    help='will assume yes if asked about overwriting files')

parser.add_argument('-d',
                    dest='depth',
                    default=16,
                    type=int,
                    help='depth in bit of the input image (e.g: 12bits for a prosilica)')

args = parser.parse_args()

outdir = args.outdir[0]
input_f = args.input_f[:]
force = args.force
depth = args.depth
input_files = []


print('================================')
print('================================')

if len(input_f) == 1:
    # directory or file
    if os.path.isdir(input_f[0]):
        print(f'Looking for files in directory {input_f[0]}')
        input_files = glob.glob(os.path.join(input_f[0], '*.edf'))
    else:
        input_files = input_f
else:
    # list of files
    input_files = [f for f in input_f if not os.path.isdir(f) and f.endswith('.edf')]

n_input_files = len(input_files)
print(f'Input: {n_input_files} files.')

if len(input_files) == 0:
    print("No files, exiting.")
    exit(0)

n_overwrite = 0
overwrite = []
for in_f in input_files:
    out_f = os.path.join(outdir, os.path.splitext(os.path.basename(in_f))[0]) + '.jpg'
    if os.path.exists(out_f):
        overwrite.append(out_f)
n_overwrite = len(overwrite)

if n_overwrite > 0:
    if force:
        print(f'WARNING! {n_overwrite} files will be overwritten')
        print('File(s) that will be overwritten:')
        print('- ' + '\n - '.join(overwrite))
        print('User decided to ignore.')
    else:
        answer = None 
        while answer not in ("yes", "no", "y", "n"): 
            print(f'WARNING! {n_overwrite} files will be overwritten. Continue anyway?')
            answer = input("Enter yes (y), no (n) or l to list the files: ").lower()
            if answer == "l": 
                print('File(s) that will be overwritten:')
                print(' - ' + '\n - '.join(overwrite))
            elif answer in ("yes", "y"): 
                # Do this. 
                pass
            elif answer in ("no", "n"): 
                print("Cancelled.")
                exit(0)
            else: 
                print("Please enter yes/y or no/n.") 

print('================================')
print('================================')
print('Starting conversion.')

for i_in_f, in_f in enumerate(input_files):
    out_f = os.path.join(outdir, os.path.splitext(os.path.basename(in_f))[0]) + '.jpg'
    print(f'Converting file {i_in_f + 1}/{n_input_files}. {in_f} -> {out_f}.')
    with silx_io.open(in_f) as edf_f:
        keys = list(edf_f.keys())
        if len(keys) > 1:
            raise RuntimeError(f'Edf file {in_f} has more than one entry.')
        image = edf_f[keys[0]]['image']['data'][:]
    if depth != 8:
        image = image * ((2**8-1) / (2**(depth) - 1))
    img2 = image.astype(np.uint8)
    
    cv2.imwrite(out_f, image.astype(np.uint8))
