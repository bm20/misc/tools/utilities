import os
import sys
import glob
import argparse

import numpy as np

import cv2
import silx
from silx import io as silx_io


spec_f = '/data/bm20/inhouse/XRD2/2021/2021-08-BLC/ylid-RT2/ylid-RT2.spec'
out_f = os.path.splitext(spec_f)[0] + '.jpr'

# if os.path.exists(out_f):
#     raise RuntimeError(f'File {out_f} already exists.')


print(f'Writing to file {out_f}')


with silx.io.open(spec_f) as spec_h5:
    keys = list(spec_h5.keys())
    if len(keys) != 1:
        raise RuntimeError(f'Expected only one scan in file. Found {keys}.')

    phi = spec_h5[keys[0]]['measurement/phi'][:]

phi = np.around(phi, decimals=4)
n_images = phi.shape[0]

omega = np.full((n_images,), 90.)
theta = np.zeros((n_images,))
kappa = np.zeros((n_images,))

with open(out_f, 'w') as out_d:
    out_d.write(f'KM4ABS version  1.000 # of JPEG images:     {n_images}\n')
    out_d.write('MICROSCOPE ORIENTATION: ...\n')
    for i_img in range(n_images):
        line = (f'#{i_img+1:> 6} o:{omega[i_img]:> 11.5f}'
                f' t: {theta[i_img]:> 11.5f}'
                f' k: {kappa[i_img]:> 11.5f}'
                f' p: {phi[i_img]:> 11.5f}\n')
        out_d.write(line)